<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Loader.php";

class MY_Loader extends MX_Loader {

	 public function template($template_name, $vars = array(), $return = FALSE)
	    {

	    	$session['session']=array();
			$session['session']=$this->session->userdata;

			//Total Kapal
			$CountKapal = $this->db->query("select count(*) as total from data_kapal");
			$totalKapal = $CountKapal->result();
			$res['totalKapal']  = $totalKapal[0]->total;

			//Total Kedatangan
			$CountCars = $this->db->query("SELECT COUNT(DISTINCT id_kapal) as total FROM data_kedatangan");
			$totalCars = $CountCars->result();
			$res['Kedatangan']  = $totalCars[0]->total;

			//Total Keberangkatan
			$CountDrivers = $this->db->query("SELECT COUNT(DISTINCT id_kapal) as total FROM data_keberangkatan");
			$totalDrivers = $CountDrivers->result();
			$res['totalKeberangkatan']  = $totalDrivers[0]->total;
			
			

			
			//Total Ikan Bulan Ini
			$CountIkan= $this->db->query("SELECT SUM(berat1+berat2+berat3+berat4+berat5) AS total_ikan FROM data_kedatangan WHERE
										 (MONTH(tanggal) = MONTH(NOW()) AND YEAR(tanggal) = YEAR(NOW()))");
			$totalIkan = $CountIkan->result();
			$res['totalIkan']  = $totalIkan[0]->total_ikan;


	 		$res['arraymenu']   = array();         
			foreach($session['session']['menu'] as $val){
			   if($val['parent_id'] != 0){
			       $res['arraymenu'][$val['parent_id']]['childnya'][] = $val;
			   }
			   else{
			       $res['arraymenu'][$val['menu_id']] = $val;
			   }
			}
			//pr($res['arraymenu']);exit;
	        $content  = $this->view($GLOBALS['site_theme']."/bg_header",$session);
	        $content  = $this->view($GLOBALS['site_theme']."/bg_left",$res);
	        $content .= $this->view($template_name, $vars, $return);
	        $content .= $this->view($GLOBALS['site_theme']."/bg_footer");
	        if ($return)
	        {
	            return $content;
	        }
	    }
		
}